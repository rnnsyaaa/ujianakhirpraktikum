package Contest;

class CAT8 extends TiketKonser {
    //Do your magic here...
    public CAT8(String name, double price) {
        super(name, price);
    }
    // 'super' digunakan untuk memanggil constructor dari superclass 'TiketKonser',
// agar bisa memanggil nama dan harga
    @Override //digunakan untuk menandakan bahwa metode berikutnya  metode yang di-override dari superclass atau interface
    public double hitungHarga() {
        return price;
    }//mengembalikan nilai dari variabel harga
}