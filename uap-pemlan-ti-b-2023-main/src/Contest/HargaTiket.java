package Contest;

interface HargaTiket {
    //Do your magic here...
    //Interface dideklarasikan untuk mendefinisikan metode yang harus diimplementasikan oleh kelas-kelas yang mengimplementasikan interface .
    double hitungHarga();
}