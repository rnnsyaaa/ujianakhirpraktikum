package Contest;

class PemesananTiket {
    // Do your magic here...
    private static TiketKonser[] tiketKonser;
    //digunakan untuk isi objek-objek tiket konser yang sesuai dengan jenis tiket yang tersedia

    static {
        tiketKonser = new TiketKonser[]{
                new CAT8("CAT 8", 8000000),
                new CAT1("CAT 1", 12000000),
                new FESTIVAL("FESTIVAL", 16500000),
                new VIP("VIP", 18000000),
                new VVIP("UNLIMITED EXPERIENCE", 20000000)
        };
    }

    public static TiketKonser pilihTiket(int indeks) {
        return tiketKonser[indeks];
    }
    // digunakan untuk memilih tiket konser berdasarkan indeks yang diberikan
}