package Contest;

class CAT1 extends TiketKonser {
    //Do your magic here...
    public CAT1(String name, double price) {
        super(name, price);
    }
// 'super' digunakan untuk memanggil constructor dari superclass 'TiketKonser',
// untuk memanggil nama dan harga
    @Override //digunakan untuk menandakan bahwa metode berikutnya  metode yang di-override dari superclass atau interface
    public double hitungHarga() {
        return price;
    } //mengembalikan nilai dari variabel harga
}