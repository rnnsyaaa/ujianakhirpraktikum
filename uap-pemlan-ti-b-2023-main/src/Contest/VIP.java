package Contest;

class VIP extends TiketKonser {
    // Do your magic here...
         public VIP(String name, double price) {
            super(name, price);
        }

        @Override //digunakan untuk menandakan bahwa metode berikutnya  metode yang di-override dari superclass atau interface
        public double hitungHarga() {
            return price;
        }//diimplementasikan ulang (override) dari kelas induk TiketKonser
    }
