/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...
        Scanner sc = new Scanner(System.in);

        System.out.println("Selamat datang di Pemesanan Tiket Coldplay!");

        try {
            System.out.print("Masukkan nama pemesan: ");
            String name = sc.nextLine();

            if (name.length() > 10) {
                throw new InvalidInputException("Panjang nama pemesan maksimal 10 karakter!");
            }

            System.out.println("Pilih jenis tiket:");
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");
            System.out.print("Masukkan Pilihan: ");

            int choice;
            try {
                choice = Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                throw new InvalidInputException("Input tidak valid! Masukkan pilihan tiket dengan angka.");
            } // try and catch digunakan untuk memblok yang digunakan jika sedang terjadi exception
            // jika hal tersebut terjadi,InvalidInputException yang telah di definisikan sebelumnya dapat digunakan.
            //
            if (choice < 1 || choice > 5) {
                throw new InvalidInputException("Input tidak valid! Pilihan hanya 1-5.");
            } // digunakan untuk memvalidasi input dari user,jika input antara 1-5 maka valid, jika tidak maka akan digunakan InvalidInputException


            TiketKonser ticket = PemesananTiket.pilihTiket(choice - 1);

            String bookingCode = generateKodeBooking();

            String tanggalPesanan = getCurrentDate(); // untuk mengambil tanggal pemesanan

            System.out.println("\n----- Detail Pemesanan -----");
            System.out.println("Nama Pemesan: " + name);
            System.out.println("Kode Booking: " + bookingCode);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + ticket.getName());
            System.out.println("Total harga: " + ticket.hitungHarga() + " USD");
        } catch (InvalidInputException e) { //blok untuk mencetak pesan kesalahan yang ada pada exception.
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } finally {
            sc.close();
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }

}