package Contest;

class VVIP extends TiketKonser {
    // Do your magic here...
    public VVIP(String name, double price) {
        super(name, price);
    }

    @Override //digunakan untuk menandakan bahwa metode berikutnya  metode yang di-override dari superclass atau interface
    public double hitungHarga() {
        return price;
    }// mengembalikan nilai harga tiket yang telah ditetapkan saat objek dibuat, tanpa ada perhitungan tambahan.
}