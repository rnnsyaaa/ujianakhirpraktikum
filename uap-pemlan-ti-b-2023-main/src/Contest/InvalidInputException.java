package Contest;

class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
} // kelas ini merupakan jenis  pengecualian yang dapat kita buat
// InvalidInputException menerima satu parameter yaitu message dengan tipe data String di baris super(message)
// digunakan untuk menginisialisasi exception dengan pesan yang dapat digunakan untuk menjelaskan kesalahan atau kondisi  tidak valid.