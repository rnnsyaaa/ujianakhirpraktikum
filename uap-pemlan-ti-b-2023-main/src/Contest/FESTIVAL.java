package Contest;

class FESTIVAL extends TiketKonser {
    //Do your magic here...
    public FESTIVAL(String name, double price) {
        super(name, price);
    }
// 'super' digunakan untuk memanggil constructor dari superclass 'TiketKonse' agar bisa forward nama dan harga
    @Override //digunakan untuk menandakan bahwa metode berikutnya  metode yang di-override dari superclass atau interface
    public double hitungHarga() {
        return price;
    }//digunakan untuk menandakan bahwa metode 'hitungHarga()' pada kelas 'FESTIVAL' adalah override dari metode yang ada di superclass atau interface yang diimplementasikan. Metode ini digunakan untuk mengembalikan nilai harga
}