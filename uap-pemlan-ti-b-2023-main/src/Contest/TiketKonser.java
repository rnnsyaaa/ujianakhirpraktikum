package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    protected String name;
    protected double price;

    public TiketKonser(String name, double price) {
        this.name = name;
        this.price = price;
        //digunakan untuk menyimpan informasi tentang nama tiket konser dan harga tiket.
    }

    public String getName() {
        return name;// digunakan untuk mengembalikan nama tiket konser
    }

    public abstract double hitungHarga();// metode abstrak yang harus diimplementasikan oleh kelas turunannya. Metode ini digunakan untuk menghitung harga tiket konser sesuai dengan aturan yang berlaku untuk setiap jenis tiket.
}